---
author: "Danesh Manoharan"
title: "Install Gcloud Cli on Archlinux"
date: 2024-01-21T20:23:18-06:00
description: "" # Post description, shows up below title
cover:
    image: "<image path/url>" # image path/url
    alt: "<alt text>" # alt text
    caption: "<text>" # display caption under cover
    relative: true # when using page bundles set this to true
    hidden: true # only hide on current single page
images:
- 
tags:
- howto
- google-cloud
- google
type: post
lastmod: 2024-01-21T20:23:18-06:00
draft: false
---

Install from the AUR
`yay google-cloud-cli` 

Start a new terminal session or source the profile file.
`source /etc/profile.d/google-cloud-cli.sh`

Verify
```
❯ which gcloud
/opt/google-cloud-cli/bin/gcloud
❯ gcloud version
Google Cloud SDK 459.0.0
alpha 2024.01.06
beta 2024.01.06
bq 2.0.101
bundled-python3-unix 3.11.6
core 2024.01.06
gcloud-crc32c 1.0.0
gsutil 5.27
```

