---
author: "Danesh Manoharan"
title: "My Yay Cheatsheet"
date: 2024-04-28T10:36:02-05:00
description: "" # Post description, shows up below title
cover:
    image: "<image path/url>" # image path/url
    alt: "<alt text>" # alt text
    caption: "<text>" # display caption under cover
    relative: true # when using page bundles set this to true
    hidden: true # only hide on current single page
images:
- 
tags:
- 
type: post
lastmod: 2024-04-28T10:36:02-05:00
draft: false
---

Show information for installed packages and system health.

`yay -Ps`

Show available updates for packages installed from the AUR.

`yay -Qau`  

Update packages installed from the AUR.

`yay -Sau`

Update all system and installed packages.

`yay -Syu` or `yay`

Update a single package.

`yay -Sy <package>`

